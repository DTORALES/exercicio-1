#include <iostream>
#include "Cliente.h"
#include "PessoaFisica.h"
#include "PessoaJuridica.h"
#include <locale>

using namespace std;

int main()
{
    Cliente *numero1 = new PessoaFisica("Diego","Av. Benjamin","90550150","031.242.860-06");
    cout << "Pessoa Fisica: " << endl;
    numero1->OnScreenInformation();

    cout << endl;

    cout << "Pessoa Juridica: " << endl;
    Cliente *numero2 = new PessoaJuridica("TesteJuridica","endereco da empresa", "99662348", "1111.0001/222.99", "Empresa do aluno");
    numero2->OnScreenInformation();
    return 0;
}
