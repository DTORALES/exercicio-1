#ifndef CLIENTE_H
#define CLIENTE_H
#include <string>

using namespace std;

class Cliente
{
      string nome;
      string endereco;
      string telefone;

    public:

        Cliente();
        Cliente( string n, string e, string t );
        void setEndereco (string e);
        void setTelefone (string t);
        void setNome (string n);

        string getNome () {return nome;}
        string getEndereco () {return endereco;}
        string getTelefone () {return telefone;}

        ~Cliente();

        virtual void OnScreenInformation()= 0;

    protected:

};

#endif
