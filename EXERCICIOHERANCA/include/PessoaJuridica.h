#ifndef PESSOAJURIDICA_H
#define PESSOAJURIDICA_H
#include <iostream>
#include <Cliente.h>



class PessoaJuridica : public Cliente
{
    string cnpj;
    string nomeFantasia;

    public:

        PessoaJuridica ( string n, string e, string t, string c, string nf);
        void setNomeFantasia (string);
        void setcnpj (string);
        void OnScreenInformation(void);
        string getCnpj ();
        string getNomeFantasia();

        ~PessoaJuridica();

 };

#endif
