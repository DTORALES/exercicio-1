#ifndef PESSOAFISICA_H
#define PESSOAFISICA_H
#include <iostream>
#include <Cliente.h>

using namespace std;

class PessoaFisica : public Cliente
{
        string cpf;

    public:
        PessoaFisica ( string n, string e, string t, string c);
        string getCpf ();
        void OnScreenInformation(void);
        ~PessoaFisica();


};

#endif // PESSOAFISICA_H
