#include "PessoaFisica.h"


// Contrutor da classe derivada, chama o contrutor da casse base passando os parametros "n" "e" e "t" e apenas seta o valor do cpf
PessoaFisica::PessoaFisica ( string n, string e, string t, string c): Cliente(n, e, t){
    cpf = c;
}


string PessoaFisica::getCpf (){
    return cpf;}


void PessoaFisica::OnScreenInformation(void){ //implementacao da fun��o abstrata do cliente
    std::cout << "Nome: " << getNome() << endl << "Endereco: " << getEndereco() << endl
    << "Telefone: " << getTelefone() << endl << "CPF: " << getCpf() << endl;

}

PessoaFisica::~PessoaFisica()
{
  //destrutor
}
