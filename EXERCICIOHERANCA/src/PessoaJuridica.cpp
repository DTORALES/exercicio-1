#include "PessoaJuridica.h"

// Contrutor da classe derivada, chama o contrutor da casse base passando os parametros "n" "e" e "t" e apenas seta o valor do cnpj e nome fantasia
PessoaJuridica::PessoaJuridica ( string n, string e, string t, string c, string nf): Cliente(n, e, t){
    cnpj            = c;
    nomeFantasia   = nf;
}

//exercitando set
void PessoaJuridica::setNomeFantasia (string nf){
    nomeFantasia = nf;
}
/*void PessoaJuridica::setcnpj (string cn){
    cnpj = cn;
}*/
//exercitando get
string PessoaJuridica::getNomeFantasia (){
    return nomeFantasia;}


string PessoaJuridica::getCnpj (){
    return cnpj;}

void PessoaJuridica::OnScreenInformation(void){//implementacao da fun��o abstrata do cliente
    cout << "Nome: " << getNome() << endl << "Endereco: " << getEndereco() << endl
    << "Telefone: " << getTelefone() << endl << "CPF: " << getCnpj() << endl << "Nome Fantasia: " << getNomeFantasia() << endl;
}

PessoaJuridica::~PessoaJuridica()
{
    //destrutor
}
